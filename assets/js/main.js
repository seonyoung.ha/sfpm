document.addEventListener('DOMContentLoaded', function() {

    // .main-nav
    var navLinks = $('.main-nav a');
    var sections = $('.main-section');

     // Check window width on page load
     checkWindowWidth();

     // Check window width on window resize
     $(window).resize(function() {
       checkWindowWidth();
     });

     function checkWindowWidth() {
       var windowWidth = $(window).width();
       var mainNav = $(".main-nav");

       if (windowWidth < 1600) {
         mainNav.addClass("dot-nav");
       } else {
         mainNav.removeClass("dot-nav");
       }
     }
        
    $(window).on('scroll', function() {
        var currentSectionIndex = 0;
        var windowHeight = window.innerHeight; 
        var scrollTop = $(window).scrollTop();

        // Find the index of the currently visible section
        sections.each(function(index, section) {
            var rect = section.getBoundingClientRect();
            if (rect.top <= windowHeight / 2 && rect.bottom + 200 >= ( windowHeight / 2 )) {
                currentSectionIndex = index;
            }
        });

        navLinks.removeClass('active');
        navLinks.eq(currentSectionIndex).addClass('active');

        //230706
        // if (currentSectionIndex==2) {
        //     $('.main-nav').addClass('dark');
        // } else {
        //     $('.main-nav').removeClass('dark');
        // }

        if (currentSectionIndex == 2 ||
            (scrollTop >= 600 && scrollTop <= 900) ||
            (scrollTop >= 2500 && scrollTop <= 5500) ||
            (scrollTop >= 6300 && scrollTop <= 6600)) {
            $('.main-nav').addClass('dark');
        } else {
            $('.main-nav').removeClass('dark');
        }
    });
    
    // Add smooth scrolling on all links inside the navbar
    $(".main-nav a").on('click', function(event) {
        if (this.hash !== "") {
            event.preventDefault();

            var hash = this.hash;
            var hashTop = $(hash).offset().top;
            
            $('html, body').animate({
                scrollTop: hashTop
            }, 200, function(){
                if (hash==="#main03") {
                    $('.main-nav').addClass('dark');
                } else {
                    $('.main-nav').removeClass('dark');
                }
            });
        } 
    });


    // #main04
    var showButtons = document.getElementsByClassName('btn-show');
    var closeButtons = document.getElementsByClassName('btn-close-desc');
    
    for (var i = 0; i < showButtons.length; i++) {
      
        //자세히보기
        showButtons[i].addEventListener('click', function(e) {
            var parentWrapDiv = this.parentNode;
            var parentDiv = parentWrapDiv.parentNode;
            var siblingDivs = parentDiv.parentNode.getElementsByClassName('goals');
            parentDiv.classList.toggle('on');
            
            for (var j = 0; j < siblingDivs.length; j++) {
                if (siblingDivs[j] !== parentDiv) {
                    
                    siblingDivs[j].classList.remove('on');
                }
            }
            e.preventDefault();
        });

        //닫기
        closeButtons[i].addEventListener('click', function(e) {
            var parentWrapDiv = this.parentNode;
            var parentDiv = parentWrapDiv.parentNode;
            var siblingDivs = parentDiv.parentNode.getElementsByClassName('goals');
            for (var j = 0; j < siblingDivs.length; j++) {
                if (siblingDivs[j] !== parentDiv) {
                    
                }
            }
            parentDiv.classList.remove('on');
            e.preventDefault();
        });
    }

    // #main05
    var moreButtons = document.getElementsByClassName('btn-more');
    
    for (var i = 0; i < moreButtons.length; i++) {
      
        //자세히보기
        moreButtons[i].addEventListener('click', function(e) {
            var parentDiv = this.parentNode;
            var siblingDivs = parentDiv.parentNode.getElementsByClassName('method');
            parentDiv.classList.add('on');
            
            for (var j = 0; j < siblingDivs.length; j++) {
                if (siblingDivs[j] !== parentDiv) {
                    siblingDivs[j].classList.remove('on');
                }
            }
            e.preventDefault();
        });
        
    }

  });