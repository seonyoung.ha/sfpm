document.addEventListener("DOMContentLoaded", () => {
    let mainNav = document.querySelectorAll('.top-main-nav > ul > li > a');
    mainNav.forEach(function(item, i) {
        item.addEventListener('mouseover', function() {
            let subNav = this.parentNode.querySelector('.depth2-list');
            //let favoritesNav = this.parentNode.querySelector('.favorites-nav');
            // 높이값 게산을 위한 카운트
            let subNavCnt = this.parentNode.querySelectorAll('.depth2-list > li').length;
            //let favoritesNavCnt = this.parentNode.querySelectorAll('.favorites-nav li').length;
            //let favoritesNavHeight = 0;
    
            if(subNav !== null) {
                /*
                if(subNavCnt > favoritesNavCnt) {
                    favoritesNavHeight = subNavCnt * 36 + 56;
                } else if(favoritesNavCnt === 0) {
                    favoritesNavHeight = 50;
                } else {
                    favoritesNavHeight = favoritesNavCnt * 36 + 56 + 40;
                }
                // 높이값 지정
                favoritesNav.style.height = favoritesNavHeight + 'px';
                */
                this.parentNode.querySelector('.depth2-list').classList.add('on');
                //this.parentNode.querySelector('.favorites-nav').classList.add('on');
                //console.log('after :: ', favoritesNavHeight);
            } else {
                //
            }
        });
        let favorites = 0;
        item.addEventListener('mouseout', function() {
            let subNav = this.parentNode.querySelector('.depth2-list');
            //let favoritesNav = this.parentNode.querySelector('.favorites-nav');
    
            if(subNav !== null) {
                subNav.classList.remove('on');
                subNav.addEventListener('mouseover', function() {
                    this.classList.add('on');
                    //favoritesNav.classList.add('on');
                    this.parentNode.classList.add('on');
                });
                subNav.addEventListener('mouseout', function() {
                    this.classList.remove('on');
                    //favoritesNav.classList.remove('on');
                    this.parentNode.classList.remove('on');
                });
            }
            /*
            favoritesNav.classList.remove('on');
    
            favoritesNav.addEventListener('mouseover', function() {
                this.classList.add('on');
                subNav.classList.add('on');
                this.parentNode.classList.add('on');
            });
            favoritesNav.addEventListener('mouseout', function() {
                this.classList.remove('on');
                subNav.classList.remove('on');
                this.parentNode.classList.remove('on');
            });
            */
        });
    });
});