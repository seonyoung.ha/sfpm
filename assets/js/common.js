document.addEventListener("DOMContentLoaded", () => {

    // BIGIN :: header menus
    document.querySelectorAll('.header-navigation > ul > li > a').forEach(function(item, i) {
        item.addEventListener('mouseenter', function(e) {
            let subUl = this.parentNode.querySelector('ul');
            // 선택된 엘리먼트의 붐노 li의 형제 li 검색
            let _thisLi = this.parentNode;
            let sblLi   = Array.prototype.filter.call(_thisLi.parentElement.children, function(e){ return e !== _thisLi;});
            // javascript error 메세지 안나오도록 예외 처리
            sblLi.forEach(function(liItem, index) {
                if(liItem.querySelector('ul') !== null) {
                    liItem.querySelector('ul').classList.remove('on');
                }
            });
            if(subUl !== null) {
                subUl.classList.add('on');
            } else {
                return false;
            }
        });

        item.addEventListener('mouseleave', function(e) {
            let subUl   = this.parentNode.querySelector('ul');
            // 선택된 엘리먼트의 붐노 li의 형제 li 검색
            let _thisLi = this.parentNode;
            let sblLi   = Array.prototype.filter.call(_thisLi.parentElement.children, function(e){ return e !== _thisLi;});

            sblLi.forEach(function(liItem, index) {
                if(liItem.querySelector('ul') !== null) {
                    liItem.querySelector('ul').classList.remove('on');
                }
            });
            // javascript error 메세지 안나오도록 예외 처리
            if(subUl !== null) {
                subUl.addEventListener('mouseleave', function() {
                    subUl.classList.remove('on');
                });
            } else {
                return false;
            }
        });
    });
    // END :: header menus


    // BIGIN :: Aside
    document.querySelectorAll('.aside > .aside-list > li.has-sub > a').forEach(function(item, i) {
        item.addEventListener('click', function(e) {
            let subUl = this.parentNode.querySelector('ul');
            this.parentNode.classList.toggle('on');
            // 선택된 엘리먼트의 붐노 li의 형제 li 검색
            let _thisLi = this.parentNode;
            let sblLi   = Array.prototype.filter.call(_thisLi.parentElement.children, function(e){ return e !== _thisLi;});
            // javascript error 메세지 안나오도록 예외 처리
            sblLi.forEach(function(liItem, index) {
                liItem.classList.remove('on');
                if(liItem.querySelector('ul') !== null) {
                    liItem.querySelector('ul').classList.remove('on');
                }
            });
            if(subUl !== null) {
                subUl.classList.add('on');
            } else {
                return false;
            }

            e.preventDefault();
        });
    });
    // END :: Aside

    // BIGIN :: Toast
    const toastTrigger = document.getElementById('liveToastBtn')
    const toastLiveExample = document.getElementById('liveToast')
    if (toastTrigger) {
        toastTrigger.addEventListener('click', () => {
            const toast = new bootstrap.Toast(toastLiveExample)

            toast.show()
        })
    }
    // END :: Toast

});




