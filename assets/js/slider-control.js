document.addEventListener('DOMContentLoaded', function() {
    const slideSwiper2 = new Swiper(".slideSwiper2", {
        loop: true,
        autoplay: {
            delay: 10000, //230612
            disableOnInteraction: false,
        },
        
        spaceBetween: 0,
        slidesPerView: 5,
        freeMode: true,
        watchSlidesProgress: true,
        clickable: false,
        slideToClickedSlide : false,
    });

    const slideSwiper1 = new Swiper(".slideSwiper1", {
        loop: true,
        autoplay: {
            delay: 2000, //230612
            disableOnInteraction: false,
        },
        touchRatio: 0, // drag 금지
        spaceBetween: 10,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        thumbs: {
            swiper: slideSwiper2,
        },
        clickable: false,
        slideToClickedSlide : false,
        
    });

    let slideNo;
    slideSwiper1.on('transitionStart', function(e) { //230706
       slideNo = slideSwiper1.realIndex + 1;
      // document.querySelector('.circle-slider-container .wrapper .item-' + slideNo).click(); //230809
    });


    window.addEventListener('resize', function() {
        // reportWindowSize();
    });


    // Circle rotation slide control
    gsap.registerPlugin(MotionPathPlugin);
    
    const circlePath = MotionPathPlugin.convertToPath("#holder", false)[0];
    circlePath.id = "circlePath";
    document.querySelector("svg").prepend(circlePath);
    
    let items = gsap.utils.toArray(".item"),
        numItems = items.length,
        itemStep = 1 / numItems,
        wrapProgress = gsap.utils.wrap(0, 1),
        snap = gsap.utils.snap(itemStep),
        wrapTracker = gsap.utils.wrap(0, numItems),
        tracker = { item: 0 };
    
    gsap.set(items, { 
        motionPath: {
            path: circlePath,
            align: circlePath,
            alignOrigin: [0.5, 0.5],
            end: i => i / items.length
        }, 
    });
    
    const tl = gsap.timeline({ paused:true, reversed: true });
    
    tl.to('.wrapper', {
        rotation: 360, 
        transformOrigin: 'center', 
        duration: 1, 
        ease: 'none'
    });
    
    tl.to(items, {
        rotation: "-=360", 
        transformOrigin: 'center', 
        duration: 1, 
        ease: 'none',
    }, 0);
    
    tl.to(tracker, {
        item: numItems,
        duration: 1, 
        ease: 'none',
        modifiers: {
            item(value) {
            return wrapTracker(numItems - Math.round(value))
            }
        }
    }, 0);
    
    items.forEach(function (el, i) {
    
        el.addEventListener("click", function () {
            var current = tracker.item,
            activeItem = i;
    
            if (i === current) {
                return;
            }
    
            //set active item to the item that was clicked and remove active class from all items
            document.querySelector('.item.active').classList.remove('active');
            items[activeItem].classList.add('active');
        
            var diff = current - i;
    
            if (Math.abs(diff) < numItems / 2) {
                moveWheel(diff * itemStep);
            } else {
                var amt = numItems - Math.abs(diff);
    
                if (current > i) {
                    moveWheel(amt * -itemStep);
                } else {
                    moveWheel(amt * itemStep);
                }
            }
        });
    });
    
    
    function moveWheel(amount, i, index) {
    
        let progress = tl.progress();
        tl.progress(wrapProgress(snap(tl.progress() + amount)))
        let next = tracker.item;
        tl.progress(progress);
    
        document.querySelector('.item.active').classList.remove('active');
        items[next].classList.add('active');  
        
        gsap.to(tl, {
            progress: snap(tl.progress() + amount),
            modifiers: {
            progress: wrapProgress
            }
        });
    }
});
